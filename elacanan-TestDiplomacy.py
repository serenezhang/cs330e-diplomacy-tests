# write in unit tests
from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):

    def test_solve_1 (self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London')

    def test_solve_2 (self):
        r = StringIO('B Madrid Hold\nA Barcelona Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]')

    def test_solve_3 (self):
        r = StringIO('A Madrid Move London\nB Barcelona Move Madrid\nC London Move Barcelona')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A London\nB Madrid\nC Barcelona')

    def test_solve_4 (self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\n D Austin Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC Barcelona\nD [dead]')

    def test_solve_5 (self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\n D Austin Move Madrid\nE Fairfield Move Barcelona')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]')

    def test_solve_6 (self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n D Austin Support A\nE Fairfield Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London\nD Austin\nE Fairfield')

    def test_solve_7 (self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support A')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Madrid\nB [dead]\nC London')

    def test_solve_8 (self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]')

  
if __name__ == "__main__": # pragma: no cover   
    main() # pragma: no cover   
