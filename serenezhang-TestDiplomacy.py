from io import StringIO
from unittest import main, TestCase

from Diplomacy import readDiplomacy, diplomacySolve

class TestDiplomacy(TestCase):
    def test_solve_1(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid")
        w = StringIO("")
        diplomacySolve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A")
        w = StringIO("")
        diplomacySolve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC London")


    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold")
        w = StringIO("")
        diplomacySolve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Hold")


if __name__ == "__main__": #pragma: no cover
    main()
