#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/Testdiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# Testdiplomacy
# -----------

class Testdiplomacy (TestCase):

    # ----
    # eval
    # ----
    # one army test
    def test_eval_1(self):
        v = diplomacy_eval(["A Madrid Hold"])
        self.assertEqual(v, [["A","Madrid"]])
    
    # army clashing
    def test_eval_2(self):
        v = diplomacy_eval(["A Madrid Hold","B Barcelona Move Madrid","C London Support B"])
        self.assertEqual(v, [['A', '[dead]'], ['B', 'Madrid'], ['C', 'London']])
    # army clashing, support being attacked
    def test_eval_3(self):
        v = diplomacy_eval(["A Madrid Hold","B Barcelona Move Madrid","C London Support B","D Austin Move London"])
        self.assertEqual(v, [['A', '[dead]'], ['B', '[dead]'],['C', '[dead]'],['D', '[dead]']])
    # three army crash
    def test_eval_4(self):
        v = diplomacy_eval(["A Madrid Hold","B Barcelona Move Madrid","C London Move Madrid"])
        self.assertEqual(v, [['A', '[dead]'], ['B', '[dead]'],['C', '[dead]']])
    # win with most support
    def test_eval_5(self):
        v = diplomacy_eval(["A Madrid Hold","B Barcelona Move Madrid","C London Move Madrid","D Paris Support B"])
        self.assertEqual(v, [['A', '[dead]'], ['B', 'Madrid'],['C', '[dead]'],['D', 'Paris']])
    # no single army with largest support
    def test_eval_6(self):
        v = diplomacy_eval(["A Madrid Hold","B Barcelona Move Madrid","C London Move Madrid","D Paris Support B","E Austin Support A"])
        self.assertEqual(v, [['A', '[dead]'], ['B', '[dead]'], ['C', '[dead]'], ['D', 'Paris'], ['E', 'Austin']])
    # multiple support with support attacked
    def test_eval_7(self):
        v = diplomacy_eval(["A Madrid Hold","B Barcelona Move Madrid","C London Support B","D Paris Support B","E Austin Move London"])
        self.assertEqual(v, [['A', '[dead]'], ['B', 'Madrid'], ['C', '[dead]'], ['D', 'Paris'], ['E', '[dead]']])
        
    def test_eval_8(self):
        v = diplomacy_eval(["A Madrid Hold", "B Barcelona Support A", "C London Move Paris"])
        self.assertEqual(v, [['A', 'Madrid'], ['B', 'Barcelona'], ['C', 'Paris']])

    def test_eval_9(self):
        v = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B", "E Rome Support C"])
        self.assertEqual(v, [['A', '[dead]'], ['B', '[dead]'], ['C', '[dead]'], ['D', 'Paris'], ['E', 'Rome']])
    # invalid input
    def test_eval_10(self):
        v = diplomacy_eval(["A Madrid AAA"])
        self.assertEqual(v, [["A",""]])

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, [["A Madrid"]])
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [["A [dead]"],["B Madrid"],["C London"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [['A', '[dead]'], ['B', '[dead]']])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    # -----
    # solve
    # -----
    # currently wrong

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")
        
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support C\nC London Move Madrid\nD Paris Support A\nE Austin Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC Madrid\nD [dead]\nE [dead]\n")

    def test_solve_5(self):
        r = StringIO(("A Barcelona Hold\n"
                        "B London Move Barcelona\n"
                        "C Paris Support B\n"
                        "D Madrid Move Barcelona\n"
                        "E Waco Support A\n"
                        "F Berlin Support A\n"
                        "G Dallas Support B\n"
                        "H Istanbul Move Dallas\n"
                        ))
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB [dead]\nC Paris\nD [dead]\nE Waco\nF Berlin\nG [dead]\nH [dead]\n")
    def test_solve_6(self):
        r = StringIO("A Prague Hold\nB Barcelona Move Prague\nC London Move Prague\nD Paris Support B\nE Austin Support A\nF Amsterdam Move Paris")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Prague\nB [dead]\nC [dead]\nD [dead]\nE Austin\nF [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch Testdiplomacy.py >  Testdiplomacy.out 2>&1


$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> Testdiplomacy.out



$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
diplomacy.py          12      0      2      0   100%
Testdiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
